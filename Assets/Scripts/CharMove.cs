﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharMove : MonoBehaviour
{
    public AudioSource rock;
    bool canMove = true;
    public float speed;
    private Rigidbody rigidbody;
    float horizontal = 0;
    float vertical = 0;
    public Animator anim;
    

    void start()
    {
        rigidbody = GetComponent<Rigidbody>();
        
    }
    void FixedUpdate()
    {
        if (canMove == false)
        {
            if(transform.position.y > 30)
            {
                GameController.Instance().LoadScene(GameController.Instance().currentStage);
            }
            return; 
        }
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        transform.position = transform.position + new Vector3(horizontal, 0,  vertical) * speed * Time.fixedDeltaTime;
        Vector3 direct = Vector3.RotateTowards(transform.forward, new Vector3(horizontal, 0, vertical), speed , Time.fixedDeltaTime);
        transform.rotation = Quaternion.LookRotation(direct);
    }
    private void Update()
    {
        if (horizontal != 0 || vertical != 0)
        {
            anim.SetInteger("State", 1);
        }
        else anim.SetInteger("State", 0);
        if(Input.GetKeyDown(KeyCode.R))
        {
            ReloadLevel();
        }
    }
    void ReloadLevel()
    {
        GameController.Instance().LoadScene(GameController.Instance().currentStage);
    }
    public void GameOver()
    {
        canMove = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Fire")
        {
            ReloadLevel();
        }
        if(other.tag == "Statue")
        {
            rock.Play();
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Statue")
        {
            rock.Stop();
        }
    }
}
