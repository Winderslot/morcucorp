﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootPanelScript : MonoBehaviour
{
    public GameObject panel;
    public bool Sactive = true;
    public GameObject SteamTrap;
    public bool IsActivate = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Sactive == true)
        {
            SteamTrap.SetActive(true);
        }
        else if (Sactive == false)
        {
            SteamTrap.SetActive(false);
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (Sactive == true) Sactive = false;
            else if (Sactive == false) Sactive = true;
            panel.GetComponent<Light>().intensity = 50;
        }

    }
}
