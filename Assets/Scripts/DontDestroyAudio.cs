﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyAudio : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);   
    }
    private void Update()
    {
        if(GameController.Instance().currentStage == 12)
        {
            gameObject.GetComponent<AudioSource>().mute = true;
        }
    }
}
