﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Colliders : MonoBehaviour
{
    public AudioSource sound;
    public bool LastPanel;
    DoorOpener door;
    Collider col;
    public bool IsActivate;
    public GameObject panel;
    
    // Start is called before the first frame update
    void Start()
    {
        door = GameObject.FindGameObjectWithTag("Door").GetComponent<DoorOpener>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Statue")
        {   
            door.OpenDoor();                          
            panel.GetComponent<Light>().intensity = 50;
            sound.Play();
            
        }
    }
}
