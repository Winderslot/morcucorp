﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireScript : MonoBehaviour
{
    public GameObject[] fire;
    private int fireNumber;
    public float fireCooldown;
    private float time;
    public float fireDuration;
    private bool isFireCooldown;


    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (isFireCooldown && time > fireCooldown)
        {
            time = 0;
            isFireCooldown = false;
            if (fireNumber == fire.Length - 1)
            {
                fireNumber = 0;
            }
            else fireNumber++;
            for(int i = 0; i < fire.Length; i++)
            {
                if (i == fireNumber) fire[i].SetActive(true);
                else fire[i].SetActive(false);
            } 
        } else if (time > fireDuration && !isFireCooldown)
        {
            time = 0;
            isFireCooldown = true;              
        }
    }
}
