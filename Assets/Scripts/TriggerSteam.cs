﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerSteam : MonoBehaviour
{
    public GameObject SteamPanelka;
    public GameObject steamTrap;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (SteamPanelka.GetComponent<SteamPanel>().canBeActive == true)
            {
                steamTrap.SetActive(true);
            }           
            if(SteamPanelka.GetComponent<SteamPanel>().canBeActive == false) steamTrap.SetActive(false);
        }
        
    }
}
