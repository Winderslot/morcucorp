﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatueSound : MonoBehaviour
{
    public AudioSource rock;
    // Start is called before the first frame update
  
    
    void Start()
    {
        rock.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" || GetComponent<Rigidbody>().velocity != Vector3.zero)
        {
            rock.Play();
        }
        
    }
    
}
