﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamUp : MonoBehaviour
{
    public bool isActive = true;
    public float forceMultiplyer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (isActive)
            {
                other.GetComponent<Rigidbody>().AddForce(Vector3.up * forceMultiplyer, ForceMode.Impulse);
                other.GetComponent<CharMove>().GameOver();
            }
            else return;
        }
    }
}
