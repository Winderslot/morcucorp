﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    private int level;
    public AudioSource music;
    public int currentStage;
    public bool isActive;
    public static GameController _gc;
    public static GameController Instance() { return _gc; }
    private void Awake()
    {
        _gc = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            GameController.Instance().LoadScene(GameController.Instance().currentStage + 1);
        }
    }
    public void LoadScene(int id)
    {
        SceneManager.LoadScene(id);
    }
    private void OnLevelWasLoaded()
    {
        currentStage = SceneManager.GetActiveScene().buildIndex;
        if (currentStage >= 2)
        {
            music.Play();
        }
    }
    public void Quit()
    {
        Application.Quit();
    }
}
