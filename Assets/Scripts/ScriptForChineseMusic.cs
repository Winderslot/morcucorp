﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptForChineseMusic : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }
    private void Update()
    {
        if (GameController.Instance().currentStage == 2)
        {
            gameObject.GetComponent<AudioSource>().Stop();
        }
    }
}
