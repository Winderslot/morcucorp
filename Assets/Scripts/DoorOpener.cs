﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DoorOpener : MonoBehaviour
{
    public AudioSource door;
    public bool isClosed = true;
    Animator doorAnim;
    private void Start()
    {
        doorAnim = GetComponent<Animator>();
    }
    public void OpenDoor()
    {
        isClosed = false;
        doorAnim.SetInteger("State", 1);
        door.Play();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && isClosed == false)
        {
            GameController.Instance().LoadScene(GameController.Instance().currentStage + 1);
        }
    }
}
