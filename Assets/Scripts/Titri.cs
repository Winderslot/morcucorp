﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Titri : MonoBehaviour
{    
    public GameObject[] pics;
    private int currentPic = 0;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentPic++;
            if (currentPic == pics.Length)
            {
                print(34);
                GameController.Instance().LoadScene(GameController.Instance().currentStage + 1);
                return;
            } 
            foreach (GameObject g in pics)
            {
                g.SetActive(false);
            }
            pics[currentPic].SetActive(true);
        }
        
    }

}
