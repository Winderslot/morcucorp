﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SteamPanel : MonoBehaviour
{
    public AudioSource steamPanelSound;
    public bool canBeActive = true;
    public GameObject steamTrap;
    public bool IsActivate = false;
    public GameObject panel;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Statue")
        {
            print(1);
            steamTrap.SetActive(false);
            panel.GetComponent<Light>().intensity = 50;
            canBeActive = false;
            steamPanelSound.Play();
        }            
    }
    private void OnTriggerExit(Collider other)
    {
        steamTrap.SetActive(true);
        panel.GetComponent<Light>().intensity = 0;
        canBeActive = true;
    }
}
