﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimplePanelka : MonoBehaviour
{
    public AudioSource sound;
    public GameObject panel;
    public GameObject Trap;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Statue")
        {
            Trap.SetActive(true);
            panel.GetComponent<Light>().intensity = 50;
            sound.Play();
        }
    }
}
